package com.msadal.mtransfer.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.msadal.mtransfer.dao.CustomerDao;
import com.msadal.mtransfer.dao.DaoFactory;
import com.msadal.mtransfer.domain.Customer;
import com.msadal.mtransfer.store.exception.NonExistingEntityException;
import com.msadal.mtransfer.store.exception.StoreException;
import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.List;

@Path("customer")
public class CustomerService {

    final static Logger log = Logger.getLogger(CustomerService.class);

    private final static GsonBuilder gBuilder = new GsonBuilder();
    private CustomerDao dao;

    public CustomerService() {
        gBuilder.setPrettyPrinting();
        dao = DaoFactory.createCustomerDao();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomersList(@Context Request request) {
        List<Customer> customers = dao.findAll();
        return Response.ok(gBuilder.create().toJson(customers)).build();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomer(@Context Request request, @PathParam("id") String customerId) {
        Customer customer = dao.find(customerId);
        return Response.ok(gBuilder.create().toJson(customer)).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createCustomer(@Context Request request, String customerJson) {

        Gson gson = gBuilder.create();
        try {
            Customer customer = gson.fromJson(customerJson, Customer.class);
            customer = dao.create(customer);
            log.debug("Customer created [id: " + customer.getId() +"]");
            return Response.ok(gson.toJson(customer)).build();
        } catch (StoreException e) {
            Status status = NonExistingEntityException.class.isInstance(e)? Status.BAD_REQUEST: Status.INTERNAL_SERVER_ERROR;
            ServiceError error = new ServiceError(status.getStatusCode(), e.getMessage());
            return Response.accepted(gson.toJson(error)).status(status).build();
        }
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateCustomer(@Context Request request, String customerJson) {
        Gson gson = gBuilder.create();
        try {
            Customer customer = gson.fromJson(customerJson, Customer.class);
            customer = dao.update(customer);
            log.debug("Customer updated [id: " + customer.getId() +"]");
            return Response.ok(gson.toJson(customer)).build();
        } catch (StoreException e) {
            Status status = NonExistingEntityException.class.isInstance(e)? Status.BAD_REQUEST: Status.INTERNAL_SERVER_ERROR;
            ServiceError error = new ServiceError(status.getStatusCode(), e.getMessage());
            return Response.accepted(gson.toJson(error)).status(status).build();
        }
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response removeCustomer(@Context Request request, @PathParam("id") String customerId) {
        try {
            dao.delete(customerId);
            log.debug("Customer removed [id: " + customerId +"]");
            return Response.ok().build();
        } catch (StoreException e) {
            Status status = NonExistingEntityException.class.isInstance(e)? Status.BAD_REQUEST: Status.INTERNAL_SERVER_ERROR;
            ServiceError error = new ServiceError(status.getStatusCode(), e.getMessage());
            return Response.accepted(gBuilder.create().toJson(error)).status(status).build();
        }
    }
}
