package com.msadal.mtransfer.service;

import java.math.BigDecimal;

public class TransactionRequest {
    private String debitAccountNr;
    private String creditAccountNr;
    private BigDecimal amount;

    public TransactionRequest(String debitAccountNr, String creditAccountNr, BigDecimal amount) {
        this.debitAccountNr = debitAccountNr;
        this.creditAccountNr = creditAccountNr;
        this.amount = amount;
    }

    public String getDebitAccountNr() {
        return debitAccountNr;
    }

    public String getCreditAccountNr() {
        return creditAccountNr;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
