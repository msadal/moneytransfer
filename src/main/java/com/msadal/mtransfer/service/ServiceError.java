package com.msadal.mtransfer.service;

public class ServiceError {
    private int statusCode;
    private String errorMessage;

    public ServiceError(int statusCode, String errorMessage) {
        this.statusCode = statusCode;
        this.errorMessage = errorMessage;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
