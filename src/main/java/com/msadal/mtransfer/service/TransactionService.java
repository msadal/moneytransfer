package com.msadal.mtransfer.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.msadal.mtransfer.dao.AccountDao;
import com.msadal.mtransfer.dao.DaoFactory;
import com.msadal.mtransfer.domain.AtomicRefAccountWrapper;
import com.msadal.mtransfer.domain.TransactionOrder;
import com.msadal.mtransfer.domain.TransactionType;
import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;

@Path("transaction")
public class TransactionService {

    final static Logger log = Logger.getLogger(TransactionService.class);
    private final static int PROCESSING_THREADS = 10;

    private final static GsonBuilder gBuilder = new GsonBuilder();
    private AccountDao accountDao;
    private TransactionManager txManager;

    public TransactionService() {
        gBuilder.setPrettyPrinting();
        accountDao = DaoFactory.createAccountDao();
        txManager = new TransactionManager(DaoFactory.createTxOrderDao(), PROCESSING_THREADS);
    }

    @POST
    @Path("transfer")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response transfer(@Context Request request, String txRequestJson) {
        Gson gson = gBuilder.create();
        TransactionRequest txRequest = gson.fromJson(txRequestJson, TransactionRequest.class);
        AtomicRefAccountWrapper debitAccountRef = accountDao.findForUpdate(txRequest.getDebitAccountNr());
        AtomicRefAccountWrapper creditAccountRef = accountDao.findForUpdate(txRequest.getCreditAccountNr());

        if (debitAccountRef == null || creditAccountRef == null ||
                txRequest.getAmount().compareTo(BigDecimal.ZERO) != 1) {
            Response.Status status = Response.Status.BAD_REQUEST;
            log.debug("Couldn't process transfer for non-existing account or amount not grater than zero");
            return Response.status(status).entity(new ServiceError(status.getStatusCode(), "Non-existing account or amount not grater than zero")).build();
        }

        TransactionOrder txOrder =
                txManager.processTransaction(txRequest, TransactionType.TRANSFER, debitAccountRef, creditAccountRef);
        log.debug("Transfer order submitted [txOrderId:" + txOrder.getId() + ", amount: " + txOrder.getAmount() + "]" );
        return Response.ok(gson.toJson(txOrder)).build();
    }

    @POST
    @Path("withdrawal")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response cashWithdrawal(@Context Request request, String txRequestJson) {
        Gson gson = gBuilder.create();
        TransactionRequest txRequest = gson.fromJson(txRequestJson, TransactionRequest.class);
        AtomicRefAccountWrapper debitAccountRef = accountDao.findForUpdate(txRequest.getDebitAccountNr());

        if (debitAccountRef == null || txRequest.getAmount().compareTo(BigDecimal.ZERO) != 1) {
            Response.Status status = Response.Status.BAD_REQUEST;
            log.debug("Couldn't process transfer for non-existing account or amount not grater than zero");
            return Response.status(status).entity(new ServiceError(status.getStatusCode(), "Non-existing account or amount not grater than zero")).build();
        }

        TransactionOrder txOrder =
                txManager.processTransaction(txRequest, TransactionType.CASH_WITHDRAWAl, debitAccountRef, null);
        log.debug("Withdrawal order submitted [txOrderId:" + txOrder.getId() + ", amount: " + txOrder.getAmount() + "]" );
        return Response.ok(gson.toJson(txOrder)).build();
    }

    @POST
    @Path("deposit")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response cashDeposit(@Context Request request, String txRequestJson) {
        Gson gson = gBuilder.create();
        TransactionRequest txRequest = gson.fromJson(txRequestJson, TransactionRequest.class);
            AtomicRefAccountWrapper creditAccountRef = accountDao.findForUpdate(txRequest.getCreditAccountNr());

            if (creditAccountRef == null || txRequest.getAmount().compareTo(BigDecimal.ZERO) != 1) {
                Response.Status status = Response.Status.BAD_REQUEST;
                log.debug("Couldn't process transfer for non-existing account or amount not grater than zero");
                return Response.status(status).entity(new ServiceError(status.getStatusCode(), "Non-existing account or amount not grater than zero")).build();
            }

        TransactionOrder txOrder = txManager.processTransaction(txRequest, TransactionType.CASH_DEPOSIT, null, creditAccountRef);
        log.debug("Deposit order submitted [txOrderId:" + txOrder.getId() + ", amount: " + txOrder.getAmount() + "]" );
        return Response.ok(gson.toJson(txOrder)).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Response getTransactionOrder(@Context Request request, @PathParam("id") String txOrderId) {
        TransactionOrder txOrder = txManager.findTransaction(txOrderId);
        return Response.ok(gBuilder.create().toJson(txOrder)).build();
    }
}
