package com.msadal.mtransfer.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.msadal.mtransfer.dao.AccountDao;
import com.msadal.mtransfer.dao.CustomerDao;
import com.msadal.mtransfer.dao.DaoFactory;
import com.msadal.mtransfer.domain.Account;
import com.msadal.mtransfer.domain.Customer;
import com.msadal.mtransfer.store.exception.NonExistingEntityException;
import com.msadal.mtransfer.store.exception.StoreException;
import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.math.BigDecimal;
import java.util.List;

@Path("/account")
public class AccountService {

    final static Logger log = Logger.getLogger(AccountService.class);

    private final static GsonBuilder gBuilder = new GsonBuilder();
    private CustomerDao customerDao;
    private AccountDao accountDao;

    public AccountService() {
        gBuilder.setPrettyPrinting();
        customerDao = DaoFactory.createCustomerDao();
        accountDao = DaoFactory.createAccountDao();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAccountsList(@Context Request request) {
        List<Account> accounts = accountDao.findAll();
        String json = gBuilder.create().toJson(accounts);
        return Response.ok(json).build();
    }

    @GET
    @Path("{number}")
    public Response getAccount(@Context Request request, @PathParam("number") String accountNumber) {
        Account account = accountDao.find(accountNumber);
        String json = gBuilder.create().toJson(account);
        return Response.ok(json).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createAccount(@Context Request request, String createRequestJson) {
        Gson gson = gBuilder.create();
        try {
            AccountCreateRequest createRequest = gson.fromJson(createRequestJson, AccountCreateRequest.class);
            Customer owner = customerDao.find(createRequest.getCustomerId());
            if (owner == null) {
                ServiceError error = new ServiceError(Status.BAD_REQUEST.getStatusCode(), "Customer with given id doesn't exist");
                log.debug("Couldn't create account for unknown customer [customerId: " + createRequest.getCustomerId() +"]");
                return Response.accepted(gson.toJson(error)).status(Status.BAD_REQUEST).build();
            }
            Account account = accountDao.create(new Account(null, owner, new BigDecimal(0)));
            log.debug("Account created [id: " + account.getNumber() +"]");
            return Response.ok(gson.toJson(account)).build();
        } catch (StoreException e) {
            Status status = NonExistingEntityException.class.isInstance(e)? Status.BAD_REQUEST: Status.INTERNAL_SERVER_ERROR;
            ServiceError error = new ServiceError(status.getStatusCode(), e.getMessage());
            return Response.accepted(gson.toJson(error)).status(status).build();
        }
    }
}
