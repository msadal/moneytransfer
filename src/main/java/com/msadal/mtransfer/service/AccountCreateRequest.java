package com.msadal.mtransfer.service;

public class AccountCreateRequest {
    private String customerId;

    public AccountCreateRequest(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerId() {
        return customerId;
    }
}
