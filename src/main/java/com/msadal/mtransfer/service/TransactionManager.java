package com.msadal.mtransfer.service;

import com.msadal.mtransfer.dao.TransactionOrderDao;
import com.msadal.mtransfer.domain.*;
import com.msadal.mtransfer.service.exception.NotEnoughFundsException;
import com.msadal.mtransfer.store.exception.StoreException;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.msadal.mtransfer.domain.AccountTransaction.TxDirection.CREDIT;
import static com.msadal.mtransfer.domain.AccountTransaction.TxDirection.DEBIT;

public class TransactionManager {

    final static Logger log = Logger.getLogger(TransactionManager.class);

    private TransactionOrderDao txOrderDao;
    private ExecutorService txProcessingExecutor;

    public TransactionManager(TransactionOrderDao txOrderDao, int threads) {
        this.txOrderDao = txOrderDao;
        this.txProcessingExecutor = Executors.newFixedThreadPool(threads);
    }

    public void shutdown() {
        txProcessingExecutor.shutdown();
    }

    public TransactionOrder findTransaction(String txId) {
        return txOrderDao.find(txId);
    }

    public TransactionOrder processTransaction(TransactionRequest txRequest, TransactionType txType,
                                                AtomicRefAccountWrapper debitAccountRef, AtomicRefAccountWrapper creditAccountRef) {

        TransactionOrder txOrder = createTxOrder(txRequest, txType);

        // process tx in separate thread
        txProcessingExecutor.submit(() -> {
            if (txType.equals(TransactionType.TRANSFER) || txType.equals(TransactionType.CASH_WITHDRAWAl)) {
                try {
                    registerDebitTx(txOrder, debitAccountRef);
                } catch (NotEnoughFundsException e) {
                    rejectTx(txOrder);
                    return;
                }
            }

            if (txType.equals(TransactionType.TRANSFER) || txType.equals(TransactionType.CASH_DEPOSIT)) {
                registerCreditTx(txOrder, creditAccountRef);
            }
            confirmTx(txOrder);
        });

        return txOrder;
    }

    private void registerDebitTx(TransactionOrder txOrder, AtomicRefAccountWrapper debitAccountRef)
            throws NotEnoughFundsException {
        AccountTransaction debitAccountTx =
                new AccountTransaction(txOrder.getAmount(), DEBIT, txOrder.getTransactionType());
        do {
            Account debitAccount = debitAccountRef.getAccount();
            if (debitAccount.getBalance().compareTo(txOrder.getAmount()) == -1) {
                throw new NotEnoughFundsException();
            }
            BigDecimal updatedBalance = debitAccount.getBalance().subtract(txOrder.getAmount());
            List<AccountTransaction> accountTxs = new LinkedList<>();
            accountTxs.addAll(debitAccount.getAccountTxs());
            accountTxs.add(debitAccountTx);
            Account updatedDebitAccount = new Account(debitAccount.getNumber(), debitAccount.getOwner(), updatedBalance, accountTxs);
            if (debitAccountRef.getRef().compareAndSet(debitAccount, updatedDebitAccount)) {
                break;
            } else {
                log.debug("Conflict registering debit accountTx for txOrder [txOrderId: " + txOrder.getId() + "] trying again");
            }
        } while (true);
        log.debug("Registered debit accountTx for txOrder [txOrderId: " + txOrder.getId() + "]");
    }

    private void registerCreditTx(TransactionOrder txOrder, AtomicRefAccountWrapper creditAccountRef) {
        AccountTransaction creditAccountTx = new AccountTransaction(txOrder.getAmount(), CREDIT, txOrder.getTransactionType());
        do {
            Account creditAccount = creditAccountRef.getAccount();
            BigDecimal updatedBalance = creditAccount.getBalance().add(txOrder.getAmount());
            List<AccountTransaction> accountTxs = new LinkedList<>();
            accountTxs.addAll(creditAccount.getAccountTxs());
            accountTxs.add(creditAccountTx);
            Account updatedCreditAccount = new Account(creditAccount.getNumber(), creditAccount.getOwner(), updatedBalance, accountTxs);
            if (creditAccountRef.getRef().compareAndSet(creditAccount, updatedCreditAccount)) {
                break;
            } else {
                log.debug("Conflict registering debit accountTx for txOrder [txOrderId: " + txOrder.getId() + "] trying again");
            }
        } while (true);
        log.debug("Registered credit accountTx for txOrder [txOrderId: " + txOrder.getId() + "]");
    }

    private TransactionOrder createTxOrder(TransactionRequest txRequest, TransactionType txType) {
        TransactionOrder txOrder = new TransactionOrder(txType, txRequest.getDebitAccountNr(), txRequest.getCreditAccountNr(), txRequest.getAmount());
        try {
            return txOrderDao.create(txOrder);
        } catch (StoreException e) {
            throw new IllegalStateException();
        }
    }

    private TransactionOrder updateTxStatus(TransactionOrder txOrder, TransactionOrderStatus newStatus) {
        try {
            TransactionOrder updatedTxOrder = new TransactionOrder(
                    txOrder.getId(),
                    txOrder.getTransactionType(),
                    txOrder.getDebitAccountNr(),
                    txOrder.getCreditAccountNr(),
                    txOrder.getAmount(),
                    newStatus
            );
            return txOrderDao.update(updatedTxOrder);
        } catch (StoreException e) {
            throw new IllegalStateException();
        }
    }

    private void rejectTx(TransactionOrder txOrder) {
        log.debug("Transaction rejected [txOrderId: " + txOrder.getId() + "]");
        updateTxStatus(txOrder, TransactionOrderStatus.REJECTED);
    }

    private void confirmTx(TransactionOrder txOrder) {
        log.debug("Transaction confirmed [txOrderId: " + txOrder.getId() + "]");
        updateTxStatus(txOrder, TransactionOrderStatus.SUCCESS);
    }
}
