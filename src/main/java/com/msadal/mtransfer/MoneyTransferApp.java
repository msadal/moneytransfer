package com.msadal.mtransfer;

import com.msadal.mtransfer.service.AccountService;
import com.msadal.mtransfer.service.CustomerService;
import com.msadal.mtransfer.service.TransactionService;
import com.sun.net.httpserver.HttpServer;
import org.apache.log4j.Logger;
import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class MoneyTransferApp {

    final static Logger logger = Logger.getLogger(MoneyTransferApp.class);

    public final static String URL = "http://localhost/";
    public final static int PORT = 8080;

    private List<Class> endpoints = new ArrayList<>();
    private AtomicReference<HttpServer> serverHook = new AtomicReference<HttpServer>();

    public MoneyTransferApp() {
        endpoints.add(CustomerService.class);
        endpoints.add(AccountService.class);
        endpoints.add(TransactionService.class);
    }

    private void runServer() {
        logger.info("Starting MoneyTransferApp server.");

        URI baseURI = UriBuilder.fromUri(URL).port(PORT).build();
        ResourceConfig config = new ResourceConfig();
        endpoints.forEach(endpointClass -> config.register(endpointClass));
        serverHook.set(JdkHttpServerFactory.createHttpServer(baseURI, config));

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            HttpServer httpServer = serverHook.get();
            if (httpServer != null) {
                logger.info("Stopping down MoneyTransferApp server.");
                httpServer.stop(0);
            }
        }));
    }

    public static void main(String[] args) {
        new MoneyTransferApp().runServer();
    }
}
