package com.msadal.mtransfer.store;

public class StoresHolder {
    private final static CustomerStore customerStore = new CustomerStore();
    private final static AccountStore accountStore = new AccountStore();
    private final static TransactionOrderStore txOrderStore = new TransactionOrderStore();

    public static CustomerStore getCustomerStore() {
        return customerStore;
    }

    public static AccountStore getAccountStore() {
        return accountStore;
    }

    public static TransactionOrderStore getTxOrderStore() {
        return txOrderStore;
    }
}
