package com.msadal.mtransfer.store;

import com.msadal.mtransfer.domain.TransactionOrder;

public class TransactionOrderStore extends ImmutableEntityStoreBase<TransactionOrder> {

    @Override
    protected String formatKey(Long sequenceValue) {
        return sequenceValue.toString();
    }
}
