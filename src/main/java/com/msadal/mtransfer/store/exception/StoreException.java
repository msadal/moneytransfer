package com.msadal.mtransfer.store.exception;

public class StoreException extends Exception {

    public StoreException(String message) {
        super(message);
    }
}
