package com.msadal.mtransfer.store.exception;

public class NonExistingEntityException extends StoreException {

    private Class entityClass;
    private String key;

    public NonExistingEntityException(String message, Class entityClass, String key) {
        super(message);
        this.entityClass = entityClass;
        this.key = key;
    }
}
