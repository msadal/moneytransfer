package com.msadal.mtransfer.store;

import com.msadal.mtransfer.domain.Account;
import com.msadal.mtransfer.domain.AtomicRefAccountWrapper;

import java.text.NumberFormat;

public class AccountStore extends ImmutableEntityStoreBase<AtomicRefAccountWrapper> {

    private final static String NUMBER_PREFIX = "UK";
    private final NumberFormat keyFormat = NumberFormat.getIntegerInstance();

    public AccountStore() {
        keyFormat.setGroupingUsed(false);
        keyFormat.setMinimumIntegerDigits(20);
    }

    @Override
    protected String formatKey(Long sequenceValue) {
        return NUMBER_PREFIX + keyFormat.format(sequenceValue);
    }
}
