package com.msadal.mtransfer.store;

import com.msadal.mtransfer.domain.Customer;
import com.msadal.mtransfer.domain.Entity;

import java.text.NumberFormat;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

public abstract class ImmutableEntityStoreBase<E extends Entity> {

    private final AtomicLong sequence = new AtomicLong(1);
    private final Map<String, E> storeMap = new ConcurrentHashMap<>();

    protected abstract String formatKey(Long sequenceValue);

    public String genNextKey() {
        return formatKey(sequence.getAndIncrement());
    }

    public Map<String, E> getStoreMap() {
        return storeMap;
    }
}
