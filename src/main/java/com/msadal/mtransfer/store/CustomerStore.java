package com.msadal.mtransfer.store;

import com.msadal.mtransfer.domain.Customer;

import java.text.NumberFormat;

public class CustomerStore extends ImmutableEntityStoreBase<Customer>  {

    private final NumberFormat keyFormat = NumberFormat.getIntegerInstance();

    public CustomerStore() {
        keyFormat.setGroupingUsed(false);
    }

    @Override
    protected String formatKey(Long sequenceValue) {
        return keyFormat.format(sequenceValue);
    }
}
