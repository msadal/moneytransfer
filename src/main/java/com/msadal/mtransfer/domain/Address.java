package com.msadal.mtransfer.domain;

public class Address {

    private String postCode;
    private String addressLine;
    private String city;

    public Address(String postCode, String addressLine, String city) {
        this.postCode = postCode;
        this.addressLine = addressLine;
        this.city = city;
    }

    public String getPostCode() {
        return postCode;
    }

    public String getAddressLine() {
        return addressLine;
    }

    public String getCity() {
        return city;
    }
}
