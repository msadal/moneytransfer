package com.msadal.mtransfer.domain;

public interface Entity {
    String getKey();
}
