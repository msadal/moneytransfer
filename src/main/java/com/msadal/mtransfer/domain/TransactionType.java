package com.msadal.mtransfer.domain;

public enum TransactionType {

    TRANSFER,
    CASH_WITHDRAWAl,
    CASH_DEPOSIT
}
