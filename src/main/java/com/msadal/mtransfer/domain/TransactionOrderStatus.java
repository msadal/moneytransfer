package com.msadal.mtransfer.domain;

public enum TransactionOrderStatus {
    PENDING,
    SUCCESS,
    REJECTED
}
