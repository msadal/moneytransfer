package com.msadal.mtransfer.domain;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Account implements Entity {

    private String number;
    private Customer owner;
    private BigDecimal balance;
    private List<AccountTransaction> accountTxs = Collections.unmodifiableList(new LinkedList<>());

    public Account(String number, Customer owner, BigDecimal balance) {
        this.number = number;
        this.owner = owner;
        this.balance = balance;
    }

    public Account(String number, Customer owner, BigDecimal balance, List<AccountTransaction> accountTxs) {
        this.number = number;
        this.owner = owner;
        this.balance = balance;
        this.accountTxs = Collections.unmodifiableList(new LinkedList<>(accountTxs));
    }

    public String getNumber() {
        return number;
    }

    public Customer getOwner() {
        return owner;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public List<AccountTransaction> getAccountTxs() {
        return accountTxs;
    }

    @Override
    public String getKey() {
        return number;
    }
}
