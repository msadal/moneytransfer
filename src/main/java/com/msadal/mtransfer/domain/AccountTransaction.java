package com.msadal.mtransfer.domain;

import java.math.BigDecimal;

public class AccountTransaction {

    public enum TxDirection {
        CREDIT,
        DEBIT
    }

    private BigDecimal amount;
    private TxDirection direction;
    private TransactionType type;

    public AccountTransaction(BigDecimal amount, TxDirection direction, TransactionType type) {
        this.amount = amount;
        this.direction = direction;
        this.type = type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public TxDirection getDirection() {
        return direction;
    }

    public TransactionType getType() {
        return type;
    }
}
