package com.msadal.mtransfer.domain;

import java.util.concurrent.atomic.AtomicReference;

public class AtomicRefAccountWrapper implements Entity {

    private AtomicReference<Account> ref;

    public AtomicRefAccountWrapper(Account account) {
        this.ref = new AtomicReference<>(account);
    }

    public Account getAccount() {
        return ref.get();
    }

    public void setAccount(Account account) {
        ref.set(account);
    }

    public AtomicReference<Account> getRef() {
        return ref;
    }

    @Override
    public String getKey() {
        Account account = ref.get();
        return account.getKey();
    }
}
