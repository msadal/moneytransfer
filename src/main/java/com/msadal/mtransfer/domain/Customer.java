package com.msadal.mtransfer.domain;

public class Customer implements Entity {

    private String id;
    private String forename;
    private String surname;
    private Address address;

    public Customer(String id, String forename, String surname, Address address) {
        this.id = id;
        this.forename = forename;
        this.surname = surname;
        this.address = address;
    }

    public String getForename() {
        return forename;
    }

    public String getSurname() {
        return surname;
    }

    public Address getAddress() {
        return address;
    }

    public String getId() {
        return id;
    }

    @Override
    public String getKey() {
        return id;
    }
}
