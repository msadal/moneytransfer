package com.msadal.mtransfer.domain;

import java.math.BigDecimal;

public class TransactionOrder implements Entity {

    private String id;
    private TransactionType transactionType;
    private String debitAccountNr;
    private String creditAccountNr;
    private BigDecimal amount;
    private TransactionOrderStatus status;

    public TransactionOrder(TransactionType transactionType, String debitAccountNr, String creditAccountNr, BigDecimal amount) {
        this(null, transactionType, debitAccountNr, creditAccountNr, amount, TransactionOrderStatus.PENDING);
    }

    public TransactionOrder(String id, TransactionType transactionType, String debitAccountNr, String creditAccountNr, BigDecimal amount, TransactionOrderStatus status) {
        this.id = id;
        this.transactionType = transactionType;
        this.debitAccountNr = debitAccountNr;
        this.creditAccountNr = creditAccountNr;
        this.amount = amount;
        this.status = status;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public String getDebitAccountNr() {
        return debitAccountNr;
    }

    public String getCreditAccountNr() {
        return creditAccountNr;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getId() {
        return id;
    }

    public TransactionOrderStatus getStatus() {
        return status;
    }

    @Override
    public String getKey() {
        return id;
    }
}
