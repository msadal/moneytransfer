package com.msadal.mtransfer.dao.impl;

import com.google.gson.reflect.TypeToken;
import com.msadal.mtransfer.dao.BaseEntityDao;

import com.msadal.mtransfer.domain.Entity;
import com.msadal.mtransfer.store.ImmutableEntityStoreBase;
import com.msadal.mtransfer.store.exception.NonExistingEntityException;
import com.msadal.mtransfer.store.exception.StoreException;

import java.util.LinkedList;
import java.util.List;

public abstract class BaseEntityDaoImpl<E extends Entity> implements BaseEntityDao<E> {

    private ImmutableEntityStoreBase<E> store;

    public BaseEntityDaoImpl(ImmutableEntityStoreBase<E> store) {
        this.store = store;
    }

    @Override
    public E update(E entity) throws StoreException {
        if (entity.getKey() != null) {
            try {
                return store.getStoreMap().compute(entity.getKey(), (key, oldValue) -> {
                    if (oldValue == null) {
                        throw new IllegalStateException();
                    }
                    return entity;
                });
            } catch (IllegalStateException e) {
                throw new NonExistingEntityException("Trying update non-existing entity", entity.getClass(), entity.getKey());
            }
        }
        throw new NonExistingEntityException("Trying update non-existing entity", entity.getClass(), null);
    }

    public E create(E customer) throws StoreException {
        String key = store.genNextKey();
        E newEntity = cloneEntityWithKey(key, customer);
        store.getStoreMap().put(key, newEntity);
        return newEntity;
    }

    @Override
    public void delete(String key) throws NonExistingEntityException {
        try {
            store.getStoreMap().compute(key, (k, oldValue) -> {
                if (oldValue == null) {
                    throw new IllegalStateException();
                }
                return null;
            });
        } catch (IllegalStateException e) {
            throw new NonExistingEntityException("Trying delete non-existing entity", new TypeToken<E>(){}.getClass(), key);
        }
    }

    @Override
    public E find(String key) {
        return store.getStoreMap().get(key);
    }

    @Override
    public List<E> findAll() {
        return new LinkedList<E>(store.getStoreMap().values());
    }

    protected abstract E cloneEntityWithKey(String key, E entity);
}
