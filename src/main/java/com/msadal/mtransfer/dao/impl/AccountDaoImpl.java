package com.msadal.mtransfer.dao.impl;

import com.msadal.mtransfer.dao.AccountDao;
import com.msadal.mtransfer.domain.Account;
import com.msadal.mtransfer.domain.AtomicRefAccountWrapper;
import com.msadal.mtransfer.store.AccountStore;
import com.msadal.mtransfer.store.exception.StoreException;

import java.util.List;
import java.util.stream.Collectors;

public class AccountDaoImpl implements AccountDao {

    private AtomicRefAccountDaoImpl dao;

    private static class AtomicRefAccountDaoImpl extends BaseEntityDaoImpl<AtomicRefAccountWrapper> {

        public AtomicRefAccountDaoImpl(AccountStore store) {
            super(store);
        }

        @Override
        protected AtomicRefAccountWrapper cloneEntityWithKey(String key, AtomicRefAccountWrapper entity) {
            Account account = entity.getAccount();
            entity.setAccount(new Account(key, account.getOwner(), account.getBalance()));
            return entity;
        }
    }

    public AccountDaoImpl(AccountStore store) {
        this.dao = new AtomicRefAccountDaoImpl(store);
    }

    @Override
    public Account create(Account entity) throws StoreException {
        return dao.create(new AtomicRefAccountWrapper(entity)).getAccount();
    }

    @Override
    public Account find(String key) {
        AtomicRefAccountWrapper wrapperAccount = dao.find(key);
        return wrapperAccount != null? wrapperAccount.getAccount(): null;
    }

    @Override
    public AtomicRefAccountWrapper findForUpdate(String key) {
        return dao.find(key);
    }

    @Override
    public List<Account> findAll() {
        return dao.findAll().stream()
                .map(wrappedAccount -> wrappedAccount.getAccount())
                .collect(Collectors.toList());
    }
}
