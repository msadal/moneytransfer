package com.msadal.mtransfer.dao.impl;

import com.msadal.mtransfer.dao.CustomerDao;
import com.msadal.mtransfer.domain.Customer;
import com.msadal.mtransfer.store.CustomerStore;

public class CustomerDaoImpl extends BaseEntityDaoImpl<Customer> implements CustomerDao {

    public CustomerDaoImpl(CustomerStore store) {
        super(store);
    }

    @Override
    protected Customer cloneEntityWithKey(String key, Customer entity) {
        return new Customer(key, entity.getForename(), entity.getSurname(), entity.getAddress());
    }
}
