package com.msadal.mtransfer.dao.impl;

import com.msadal.mtransfer.dao.TransactionOrderDao;
import com.msadal.mtransfer.domain.TransactionOrder;
import com.msadal.mtransfer.store.exception.NonExistingEntityException;
import com.msadal.mtransfer.store.exception.StoreException;
import com.msadal.mtransfer.store.TransactionOrderStore;

public class TransactionOrderDaoImpl extends BaseEntityDaoImpl<TransactionOrder> implements TransactionOrderDao {

    public TransactionOrderDaoImpl(TransactionOrderStore store) {
        super(store);
    }

    @Override
    protected TransactionOrder cloneEntityWithKey(String key, TransactionOrder entity) {
        return new TransactionOrder(key, entity.getTransactionType(), entity.getDebitAccountNr(), entity.getCreditAccountNr(), entity.getAmount(), entity.getStatus());
    }

    @Override
    public void delete(String key) throws NonExistingEntityException {
        throw new IllegalStateException("Method not supported.");
    }
}
