package com.msadal.mtransfer.dao;

import com.msadal.mtransfer.domain.TransactionOrder;

public interface TransactionOrderDao extends BaseEntityDao<TransactionOrder> {
}
