package com.msadal.mtransfer.dao;

import com.msadal.mtransfer.domain.Customer;

public interface CustomerDao extends BaseEntityDao<Customer> {
}
