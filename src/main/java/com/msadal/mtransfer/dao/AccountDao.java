package com.msadal.mtransfer.dao;

import com.msadal.mtransfer.domain.Account;
import com.msadal.mtransfer.domain.AtomicRefAccountWrapper;
import com.msadal.mtransfer.store.exception.StoreException;

import java.util.List;

public interface AccountDao {
    Account create(Account entity) throws StoreException;

    Account find(String key);

    AtomicRefAccountWrapper findForUpdate(String key);

    List<Account> findAll();
}
