package com.msadal.mtransfer.dao;

import com.msadal.mtransfer.domain.Entity;
import com.msadal.mtransfer.store.exception.StoreException;

import java.util.List;

public interface BaseEntityDao<E extends Entity> {

    E create(E entity) throws StoreException;

    E update(E customer) throws StoreException;

    void delete(String key) throws StoreException;

    E find(String key);

    List<E> findAll();
}
