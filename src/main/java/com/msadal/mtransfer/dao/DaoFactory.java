package com.msadal.mtransfer.dao;

import com.msadal.mtransfer.dao.impl.AccountDaoImpl;
import com.msadal.mtransfer.dao.impl.CustomerDaoImpl;
import com.msadal.mtransfer.dao.impl.TransactionOrderDaoImpl;
import com.msadal.mtransfer.store.StoresHolder;

public class DaoFactory {

    public static AccountDao createAccountDao() {
        return new AccountDaoImpl(StoresHolder.getAccountStore());
    }

    public static CustomerDao createCustomerDao() {
        return new CustomerDaoImpl(StoresHolder.getCustomerStore());
    }

    public static TransactionOrderDao createTxOrderDao() {
        return new TransactionOrderDaoImpl(StoresHolder.getTxOrderStore());
    }
}
