package com.msadal.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.msadal.mtransfer.domain.Address;
import com.msadal.mtransfer.domain.Customer;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CustomerServiceTestCase {

    private final static String SERVICE_ADDRESS = "http://localhost:8080/customer";
    private final static GsonBuilder gBuilder = new GsonBuilder();

    @BeforeClass
    public static void intiTestCase() {
        gBuilder.setPrettyPrinting();
    }

    @Test
    public void testCreateCustomer() {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(SERVICE_ADDRESS);

        Address address = new Address("SE9 6UF", "384 Well Hall Road", "London");
        Customer customer = new Customer(null, "Mariusz", "Sadal", address);
        Gson gson = gBuilder.create();

        // create
        String cJson = gson.toJson(customer);
        Response response = target.request().post(Entity.entity(cJson, MediaType.APPLICATION_JSON_TYPE));
        Customer storedCustomer = gson.fromJson(response.readEntity(String.class), Customer.class);

        assertNotNull(storedCustomer);
        assertNotNull(storedCustomer.getId());
        assertEquals("Mariusz", storedCustomer.getForename());
        assertEquals("Sadal", storedCustomer.getSurname());
        assertEquals("384 Well Hall Road", storedCustomer.getAddress().getAddressLine());
        assertEquals("SE9 6UF", storedCustomer.getAddress().getPostCode());
        assertEquals("London", storedCustomer.getAddress().getCity());
    }

    @Test
    public void testUpdateCustomer() {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(SERVICE_ADDRESS);

        Address address = new Address("SE9 6", "384 ", "L");
        Customer customer = new Customer(null, "K", "Sadal", address);
        Gson gson = gBuilder.create();

        // create
        String cJson = gson.toJson(customer);
        Response response = target.request().post(Entity.entity(cJson, MediaType.APPLICATION_JSON_TYPE));
        Customer storedCustomer = gson.fromJson(response.readEntity(String.class), Customer.class);

        address = new Address("SE9 6UF", "384 Well Hall Road", "London");
        Customer changedCustomer = new Customer(storedCustomer.getId(), "Karolina", "Sadal", address);
        // update
        cJson = gson.toJson(changedCustomer);
        response = target.request().put(Entity.entity(cJson, MediaType.APPLICATION_JSON_TYPE));
        Customer updatedCustomer = gson.fromJson(response.readEntity(String.class), Customer.class);

        assertEquals(storedCustomer.getId(), updatedCustomer.getId());
        assertEquals("Karolina", updatedCustomer.getForename());
        assertEquals("Sadal", updatedCustomer.getSurname());
        assertEquals("384 Well Hall Road", updatedCustomer.getAddress().getAddressLine());
        assertEquals("SE9 6UF", updatedCustomer.getAddress().getPostCode());
        assertEquals("London", updatedCustomer.getAddress().getCity());
    }

    @Test
    public void testFindCustomer() {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(SERVICE_ADDRESS);

        Address address = new Address("SE9 6UF", "384 Well Hall Road", "London");
        Customer customer = new Customer(null, "Kaja", "Sadal", address);
        Gson gson = gBuilder.create();

        // create
        String cJson = gson.toJson(customer);
        Response response = target.request().post(Entity.entity(cJson, MediaType.APPLICATION_JSON_TYPE));
        Customer storedCustomer = gson.fromJson(response.readEntity(String.class), Customer.class);

        response = target.path(storedCustomer.getId()).request().get();
        Customer foundCustomer = gson.fromJson(response.readEntity(String.class), Customer.class);

        assertEquals(storedCustomer.getId(), foundCustomer.getId());
        assertEquals("Kaja", foundCustomer.getForename());
        assertEquals("Sadal", foundCustomer.getSurname());
        assertEquals("384 Well Hall Road", foundCustomer.getAddress().getAddressLine());
        assertEquals("SE9 6UF", foundCustomer.getAddress().getPostCode());
        assertEquals("London", foundCustomer.getAddress().getCity());
    }
}
