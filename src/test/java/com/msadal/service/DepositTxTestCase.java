package com.msadal.service;

import com.google.gson.Gson;
import com.msadal.mtransfer.domain.*;
import com.msadal.mtransfer.service.TransactionRequest;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;

import static com.msadal.mtransfer.domain.AccountTransaction.TxDirection.CREDIT;
import static org.junit.Assert.*;

public class DepositTxTestCase extends TransactionServiceTestCaseBase {

    @Test
    public void testDeposit() throws Exception {
        Client client = ClientBuilder.newClient();
        Gson gson = gBuilder.create();

        // create account for customer
        Address address = new Address("SE9 6UF", "384 Well Hall Road", "London");
        Customer customer = new Customer(null, "Mariusz", "Sadal", address);
        Account account = createAccountForCustomer(customer);

        // deposit
        WebTarget txTarget = client.target(TRANSACTION_SERVICE);
        TransactionRequest txRequest = new TransactionRequest(null, account.getNumber(), new BigDecimal(1000));
        Response response = txTarget.path("deposit").request().post(Entity.entity(gson.toJson(txRequest), MediaType.APPLICATION_JSON_TYPE));
        TransactionOrder txOrder = gson.fromJson(response.readEntity(String.class), TransactionOrder.class);

        assertNotNull(txOrder);
        assertNotNull(txOrder.getId());
        assertEquals(account.getNumber(), txOrder.getCreditAccountNr());
        assertNull(txOrder.getDebitAccountNr());
        assertEquals(new BigDecimal(1000), txOrder.getAmount());
        assertEquals(TransactionType.CASH_DEPOSIT, txOrder.getTransactionType());

        Thread.sleep(100);
        // refresh txOrder, check state
        response = txTarget.path(txOrder.getId()).request().get();
        TransactionOrder refreshedTxOrder = gson.fromJson(response.readEntity(String.class), TransactionOrder.class);
        assertEquals(TransactionOrderStatus.SUCCESS, refreshedTxOrder.getStatus());

        // check account balance and txs
        WebTarget accountTarget = client.target(ACCOUNT_SERVICE);
        response = accountTarget.path(account.getNumber()).request().get();
        Account refreshedAccount = gson.fromJson(response.readEntity(String.class), Account.class);
        assertEquals(new BigDecimal(1000), refreshedAccount.getBalance());
        assertEquals(1, refreshedAccount.getAccountTxs().size());
        AccountTransaction tx = refreshedAccount.getAccountTxs().get(0);
        assertEquals(new BigDecimal(1000), tx.getAmount());
        assertEquals(CREDIT, tx.getDirection());
        assertEquals(TransactionType.CASH_DEPOSIT, tx.getType());
    }
}
