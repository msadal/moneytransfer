package com.msadal.service;

import com.google.gson.Gson;
import com.msadal.mtransfer.domain.*;
import com.msadal.mtransfer.service.TransactionRequest;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;

import static com.msadal.mtransfer.domain.AccountTransaction.TxDirection.CREDIT;
import static com.msadal.mtransfer.domain.AccountTransaction.TxDirection.DEBIT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TransferTxTestCase extends TransactionServiceTestCaseBase {

    @Test
    public void testTransfer() throws Exception {
        Client client = ClientBuilder.newClient();
        Gson gson = gBuilder.create();

        // create accounts for customers
        Address address = new Address("SE9 6UF", "384 Well Hall Road", "London");
        Customer customer = new Customer(null, "Kaja", "Sadal", address);
        Account fromAccount = createAccountForCustomer(customer);
        customer = new Customer(null, "Franek", "Sadal", address);
        Account toAccount = createAccountForCustomer(customer);

        // deposit
        WebTarget txTarget = client.target(TRANSACTION_SERVICE);
        TransactionRequest txRequest = new TransactionRequest(null, fromAccount.getNumber(), new BigDecimal(5000));
        txTarget.path("deposit").request().post(Entity.entity(gson.toJson(txRequest), MediaType.APPLICATION_JSON_TYPE));

        // transfer
        txTarget = client.target(TRANSACTION_SERVICE);
        txRequest = new TransactionRequest(fromAccount.getNumber(), toAccount.getNumber(), new BigDecimal(3000));
        Response response = txTarget.path("transfer").request().post(Entity.entity(gson.toJson(txRequest), MediaType.APPLICATION_JSON_TYPE));
        TransactionOrder txOrder = gson.fromJson(response.readEntity(String.class), TransactionOrder.class);

        assertNotNull(txOrder);
        assertNotNull(txOrder.getId());
        assertEquals(fromAccount.getNumber(), txOrder.getDebitAccountNr());
        assertEquals(toAccount.getNumber(), txOrder.getCreditAccountNr());
        assertEquals(new BigDecimal(3000), txOrder.getAmount());
        assertEquals(TransactionType.TRANSFER, txOrder.getTransactionType());

        Thread.sleep(100);
        // refresh txOrder, check state
        response = txTarget.path(txOrder.getId()).request().get();
        TransactionOrder refreshedTxOrder = gson.fromJson(response.readEntity(String.class), TransactionOrder.class);
        assertEquals(TransactionOrderStatus.SUCCESS, refreshedTxOrder.getStatus());

        // check account balance and txs for both accounts
        WebTarget accountTarget = client.target(ACCOUNT_SERVICE);
        response = accountTarget.path(fromAccount.getNumber()).request().get();
        Account refreshedFromAccount = gson.fromJson(response.readEntity(String.class), Account.class);
        assertEquals(new BigDecimal(2000), refreshedFromAccount.getBalance());
        assertEquals(2, refreshedFromAccount.getAccountTxs().size());
        AccountTransaction tx = refreshedFromAccount.getAccountTxs().get(1);
        assertEquals(new BigDecimal(3000), tx.getAmount());
        assertEquals(DEBIT, tx.getDirection());
        assertEquals(TransactionType.TRANSFER, tx.getType());

        accountTarget = client.target(ACCOUNT_SERVICE);
        response = accountTarget.path(toAccount.getNumber()).request().get();
        Account refreshedToAccount = gson.fromJson(response.readEntity(String.class), Account.class);
        assertEquals(new BigDecimal(3000), refreshedToAccount.getBalance());
        assertEquals(1, refreshedToAccount.getAccountTxs().size());
        tx = refreshedToAccount.getAccountTxs().get(0);
        assertEquals(new BigDecimal(3000), tx.getAmount());
        assertEquals(CREDIT, tx.getDirection());
        assertEquals(TransactionType.TRANSFER, tx.getType());
    }

    @Test
    public void testTransferReject() throws Exception {
        Client client = ClientBuilder.newClient();
        Gson gson = gBuilder.create();

        // create accounts for customers
        Address address = new Address("SE9 6UF", "384 Well Hall Road", "London");
        Customer customer = new Customer(null, "Mariusz", "Sadal", address);
        Account fromAccount = createAccountForCustomer(customer);
        customer = new Customer(null, "Jan", "Sadal", address);
        Account toAccount = createAccountForCustomer(customer);

        // deposit
        WebTarget txTarget = client.target(TRANSACTION_SERVICE);
        TransactionRequest txRequest = new TransactionRequest(null, fromAccount.getNumber(), new BigDecimal(2000));
        txTarget.path("deposit").request().post(Entity.entity(gson.toJson(txRequest), MediaType.APPLICATION_JSON_TYPE));

        // transfer
        txTarget = client.target(TRANSACTION_SERVICE);
        txRequest = new TransactionRequest(fromAccount.getNumber(), toAccount.getNumber(), new BigDecimal(2001));
        Response response = txTarget.path("transfer").request().post(Entity.entity(gson.toJson(txRequest), MediaType.APPLICATION_JSON_TYPE));
        TransactionOrder txOrder = gson.fromJson(response.readEntity(String.class), TransactionOrder.class);

        assertNotNull(txOrder);
        assertNotNull(txOrder.getId());
        assertEquals(fromAccount.getNumber(), txOrder.getDebitAccountNr());
        assertEquals(toAccount.getNumber(), txOrder.getCreditAccountNr());
        assertEquals(new BigDecimal(2001), txOrder.getAmount());
        assertEquals(TransactionType.TRANSFER, txOrder.getTransactionType());

        Thread.sleep(100);
        // refresh txOrder, check state
        response = txTarget.path(txOrder.getId()).request().get();
        TransactionOrder refreshedTxOrder = gson.fromJson(response.readEntity(String.class), TransactionOrder.class);
        assertEquals(TransactionOrderStatus.REJECTED, refreshedTxOrder.getStatus());

        // check account balance and txs for both accounts
        WebTarget accountTarget = client.target(ACCOUNT_SERVICE);
        response = accountTarget.path(fromAccount.getNumber()).request().get();
        Account refreshedFromAccount = gson.fromJson(response.readEntity(String.class), Account.class);
        assertEquals(new BigDecimal(2000), refreshedFromAccount.getBalance());
        assertEquals(1, refreshedFromAccount.getAccountTxs().size());

        accountTarget = client.target(ACCOUNT_SERVICE);
        response = accountTarget.path(toAccount.getNumber()).request().get();
        Account refreshedToAccount = gson.fromJson(response.readEntity(String.class), Account.class);
        assertEquals(new BigDecimal(0), refreshedToAccount.getBalance());
        assertEquals(0, refreshedToAccount.getAccountTxs().size());
    }
}
