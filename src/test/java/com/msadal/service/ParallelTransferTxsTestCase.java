package com.msadal.service;

import com.google.gson.Gson;
import com.msadal.mtransfer.domain.Account;
import com.msadal.mtransfer.domain.Address;
import com.msadal.mtransfer.domain.Customer;
import com.msadal.mtransfer.service.TransactionRequest;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static com.msadal.mtransfer.domain.AccountTransaction.TxDirection.CREDIT;
import static org.junit.Assert.assertEquals;

public class ParallelTransferTxsTestCase extends TransactionServiceTestCaseBase {

    private static final int MAX_TX_AMOUNT = 40;
    private static final int TX_NUMBER = 200;

    private final ExecutorService jan2FranekExecutor = Executors.newFixedThreadPool(50);
    private final ExecutorService franek2JanExecutor = Executors.newFixedThreadPool(50);

    private void sendTransfer(Account fromAccount, Account toAccount, BigDecimal amount) {
        Gson gson = gBuilder.create();
        WebTarget txTarget = ClientBuilder.newClient().target(TRANSACTION_SERVICE);
        TransactionRequest txRequest = new TransactionRequest(fromAccount.getNumber(), toAccount.getNumber(), amount);
        txTarget.path("transfer").request().post(Entity.entity(gson.toJson(txRequest), MediaType.APPLICATION_JSON_TYPE));
    }

    @Test
    public void testTransfer() throws Exception {
        int janStartBalance = 100000;
        int fraStartBalance = 50000;
        Client client = ClientBuilder.newClient();
        Gson gson = gBuilder.create();

        // create accounts for customers
        Address address = new Address("SE9 6UF", "384 Well Hall Road", "London");
        Customer customer = new Customer(null, "Jan", "Sadal", address);
        Account janAccount = createAccountForCustomer(customer);
        customer = new Customer(null, "Franek", "Sadal", address);
        Account fraAccount = createAccountForCustomer(customer);

        // deposits
        WebTarget txTarget = client.target(TRANSACTION_SERVICE);
        TransactionRequest txRequest = new TransactionRequest(null, janAccount.getNumber(), new BigDecimal(janStartBalance));
        txTarget.path("deposit").request().post(Entity.entity(gson.toJson(txRequest), MediaType.APPLICATION_JSON_TYPE));
        txTarget = client.target(TRANSACTION_SERVICE);
        txRequest = new TransactionRequest(null, fraAccount.getNumber(), new BigDecimal(fraStartBalance));
        txTarget.path("deposit").request().post(Entity.entity(gson.toJson(txRequest), MediaType.APPLICATION_JSON_TYPE));


        // send transfer in parallel
        Thread thread1 = new Thread(() -> {
            int txCounter = 0;
            int sum = 0;
            while (txCounter < TX_NUMBER) {
                Random rand = new Random(System.currentTimeMillis());
                int txAmount = rand.nextInt(MAX_TX_AMOUNT) + 1;
                sum += txAmount;
                txCounter++;
                jan2FranekExecutor.submit(() -> sendTransfer(janAccount, fraAccount, new BigDecimal(txAmount)));
            }
        });
        Thread thread2 = new Thread(() -> {
            int txCounter = 0;
            int sum = 0;
            while (txCounter < TX_NUMBER) {
                Random rand = new Random(System.currentTimeMillis());
                int txAmount = rand.nextInt(MAX_TX_AMOUNT) + 1;
                sum += txAmount;
                txCounter++;
                franek2JanExecutor.submit(() -> sendTransfer(fraAccount, janAccount, new BigDecimal(txAmount)));
            }
        });
        thread2.start();
        thread1.start();
        thread2.join();
        thread1.join();
        jan2FranekExecutor.shutdown();
        franek2JanExecutor.shutdown();

        Thread.sleep(TimeUnit.SECONDS.toMillis(10));


        // check account balance and txs for both accounts
        int txCount = 2 * TX_NUMBER + 1;
        Account refreshedJanAccount = null;
        do {
            Thread.sleep(TimeUnit.SECONDS.toMillis(2));
            WebTarget accountTarget = client.target(ACCOUNT_SERVICE);
            Response response = accountTarget.path(janAccount.getNumber()).request().get();
            refreshedJanAccount = gson.fromJson(response.readEntity(String.class), Account.class);
        } while (refreshedJanAccount.getAccountTxs().size() < txCount);

        Account refreshedFraAccount =null;
        do {
            Thread.sleep(TimeUnit.SECONDS.toMillis(2));
            WebTarget accountTarget = client.target(ACCOUNT_SERVICE);
            Response response = accountTarget.path(fraAccount.getNumber()).request().get();
            refreshedFraAccount = gson.fromJson(response.readEntity(String.class), Account.class);
        } while (refreshedFraAccount.getAccountTxs().size() < txCount);

        System.out.println("Jan balance " + refreshedJanAccount.getBalance());
        System.out.println("Franek balance " + refreshedFraAccount.getBalance());
        BigDecimal balanceSum = refreshedJanAccount.getBalance().add(refreshedFraAccount.getBalance());
        BigDecimal expectedSum = new BigDecimal(fraStartBalance + janStartBalance);
        assertEquals(expectedSum, balanceSum);

        int sum = refreshedJanAccount.getAccountTxs()
                .stream()
                .mapToInt(tx -> (tx.getDirection().equals(CREDIT)? tx.getAmount(): tx.getAmount().negate()).intValue())
                .sum();
        System.out.println("Calculated Jan balance " + sum);
        assertEquals(refreshedJanAccount.getBalance(), new BigDecimal(sum));

        sum = refreshedFraAccount.getAccountTxs()
                .stream()
                .mapToInt(tx -> (tx.getDirection().equals(CREDIT)? tx.getAmount(): tx.getAmount().negate()).intValue())
                .sum();
        System.out.println("Calculated Franek balance " + sum);
        assertEquals(refreshedFraAccount.getBalance(), new BigDecimal(sum));

    }
}
