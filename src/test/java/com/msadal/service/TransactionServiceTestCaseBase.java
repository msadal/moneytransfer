package com.msadal.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.msadal.mtransfer.domain.Account;
import com.msadal.mtransfer.domain.Customer;
import com.msadal.mtransfer.service.AccountCreateRequest;
import org.junit.Before;
import org.junit.BeforeClass;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public abstract class TransactionServiceTestCaseBase {

    protected final static String CUSTOMER_SERVICE = "http://localhost:8080/customer";
    protected final static String ACCOUNT_SERVICE = "http://localhost:8080/account";
    protected final static String TRANSACTION_SERVICE = "http://localhost:8080/transaction";
    protected final static GsonBuilder gBuilder = new GsonBuilder();
    protected Client client;

    @BeforeClass
    public static void intiTestCase() {
        gBuilder.setPrettyPrinting();
    }

    @Before
    public void initTest() {
        client = ClientBuilder.newClient();
    }

    protected Account createAccountForCustomer(Customer customer) {
        Gson gson = gBuilder.create();
        // create customer
        WebTarget customerTarget = client.target(CUSTOMER_SERVICE);
        Response response = customerTarget.request().post(Entity.entity(gson.toJson(customer), MediaType.APPLICATION_JSON_TYPE));
        Customer storedCustomer = gson.fromJson(response.readEntity(String.class), Customer.class);

        // create account
        WebTarget accountTarget = client.target(ACCOUNT_SERVICE);
        AccountCreateRequest createAccount = new AccountCreateRequest(storedCustomer.getId());
        response = accountTarget.request().post(Entity.entity(gson.toJson(createAccount), MediaType.APPLICATION_JSON_TYPE));
        return gson.fromJson(response.readEntity(String.class), Account.class);
    }
}
