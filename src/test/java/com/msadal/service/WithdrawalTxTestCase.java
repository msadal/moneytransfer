package com.msadal.service;

import com.google.gson.Gson;
import com.msadal.mtransfer.domain.*;
import com.msadal.mtransfer.service.TransactionRequest;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;

import static com.msadal.mtransfer.domain.AccountTransaction.TxDirection.DEBIT;
import static org.junit.Assert.*;

public class WithdrawalTxTestCase extends TransactionServiceTestCaseBase {

    @Test
    public void testWithdrawal() throws Exception {
        Client client = ClientBuilder.newClient();
        Gson gson = gBuilder.create();

        // create account for customer
        Address address = new Address("SE9 6UF", "384 Well Hall Road", "London");
        Customer customer = new Customer(null, "Kaja", "Sadal", address);
        Account account = createAccountForCustomer(customer);

        // deposit
        WebTarget txTarget = client.target(TRANSACTION_SERVICE);
        TransactionRequest txRequest = new TransactionRequest(null, account.getNumber(), new BigDecimal(2000));
        txTarget.path("deposit").request().post(Entity.entity(gson.toJson(txRequest), MediaType.APPLICATION_JSON_TYPE));

        // withdrawal
        txTarget = client.target(TRANSACTION_SERVICE);
        txRequest = new TransactionRequest(account.getNumber(), null, new BigDecimal(1000));
        Response response = txTarget.path("withdrawal").request().post(Entity.entity(gson.toJson(txRequest), MediaType.APPLICATION_JSON_TYPE));
        TransactionOrder txOrder = gson.fromJson(response.readEntity(String.class), TransactionOrder.class);

        assertNotNull(txOrder);
        assertNotNull(txOrder.getId());
        assertEquals(account.getNumber(), txOrder.getDebitAccountNr());
        assertNull(txOrder.getCreditAccountNr());
        assertEquals(new BigDecimal(1000), txOrder.getAmount());
        assertEquals(TransactionType.CASH_WITHDRAWAl, txOrder.getTransactionType());

        Thread.sleep(100);
        // refresh txOrder, check state
        response = txTarget.path(txOrder.getId()).request().get();
        TransactionOrder refreshedTxOrder = gson.fromJson(response.readEntity(String.class), TransactionOrder.class);
        assertEquals(TransactionOrderStatus.SUCCESS, refreshedTxOrder.getStatus());

        // check account balance and txs
        WebTarget accountTarget = client.target(ACCOUNT_SERVICE);
        response = accountTarget.path(account.getNumber()).request().get();
        Account refreshedAccount = gson.fromJson(response.readEntity(String.class), Account.class);
        assertEquals(new BigDecimal(1000), refreshedAccount.getBalance());
        assertEquals(2, refreshedAccount.getAccountTxs().size());
        AccountTransaction tx = refreshedAccount.getAccountTxs().get(1);
        assertEquals(new BigDecimal(1000), tx.getAmount());
        assertEquals(DEBIT, tx.getDirection());
        assertEquals(TransactionType.CASH_WITHDRAWAl, tx.getType());
    }

    @Test
    public void testWithdrawalRejected() throws Exception {
        Client client = ClientBuilder.newClient();
        Gson gson = gBuilder.create();

        // create account for customer
        Address address = new Address("SE9 6UF", "384 Well Hall Road", "London");
        Customer customer = new Customer(null, "Kaja", "Sadal", address);
        Account account = createAccountForCustomer(customer);

        // deposit
        WebTarget txTarget = client.target(TRANSACTION_SERVICE);
        TransactionRequest txRequest = new TransactionRequest(null, account.getNumber(), new BigDecimal(2000));
        Response response = txTarget.path("deposit").request().post(Entity.entity(gson.toJson(txRequest), MediaType.APPLICATION_JSON_TYPE));

        // withdrawal
        txTarget = client.target(TRANSACTION_SERVICE);
        txRequest = new TransactionRequest(account.getNumber(), null, new BigDecimal(3000));
        response = txTarget.path("withdrawal").request().post(Entity.entity(gson.toJson(txRequest), MediaType.APPLICATION_JSON_TYPE));
        TransactionOrder txOrder = gson.fromJson(response.readEntity(String.class), TransactionOrder.class);

        assertNotNull(txOrder);
        assertNotNull(txOrder.getId());
        assertEquals(account.getNumber(), txOrder.getDebitAccountNr());
        assertNull(txOrder.getCreditAccountNr());
        assertEquals(new BigDecimal(3000), txOrder.getAmount());
        assertEquals(TransactionType.CASH_WITHDRAWAl, txOrder.getTransactionType());

        Thread.sleep(100);
        // refresh txOrder, check state
        response = txTarget.path(txOrder.getId()).request().get();
        TransactionOrder refreshedTxOrder = gson.fromJson(response.readEntity(String.class), TransactionOrder.class);
        assertEquals(TransactionOrderStatus.REJECTED, refreshedTxOrder.getStatus());

        // check account balance and txs
        WebTarget accountTarget = client.target(ACCOUNT_SERVICE);
        response = accountTarget.path(account.getNumber()).request().get();
        Account refreshedAccount = gson.fromJson(response.readEntity(String.class), Account.class);
        assertEquals(new BigDecimal(2000), refreshedAccount.getBalance());
        assertEquals(1, refreshedAccount.getAccountTxs().size());
    }
}
